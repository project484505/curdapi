package com.example.curdAPI.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class PersonsEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String firstName;
    private String lastName;

    private  int age;

    private String companyName;

    private String city;

    private String state;

    private int pincode;

    private String email;

    private  String web;


// Constructors
     public PersonsEntity(String  firstName, String lastName, int age, String companyName, String city, String state, int pincode, String email, String Web)
     {
         this.firstName = firstName;
         this.lastName = lastName;
         this.age = age;
         this.email = email;
         this.companyName = companyName;
         this.city = city;
         this.state = state;
         this.pincode = pincode;
         this.web = web;
     }

    public PersonsEntity() {

    }

//    public void setId(int id) {
//    }

// getter and setter
//
//    public int getId(){
//         return id;
//    }
//
//    public void setId(int id){
//         this.id = id;
//    }
//
//    public  String getfirstName(){
//         return firstName;
//    }
//    public void setfirstName(String firstName){
//         this.firstName =
//    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getfirstName() {
        return firstName;
    }

    public void setfirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getlastName() {
        return lastName;
    }

    public void setlastName(String lastName) {
        this.lastName = lastName;
    }

    public String getcompanyName() {
        return companyName;
    }

    public void setcompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getcity() {
        return city;
    }

    public void setcity(String city) {
        this.city = city;
    }

    public String getstate() {
        return state;
    }

    public void setstate(String state) {
        this.state = state;
    }

    public int getPincode() {
        return pincode;
    }

    public void getpincode(int pincode) {
        this.pincode = pincode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public int getage() {
        return age;
    }

    public void setage(int age) {
        this.age = age;
    }

}
