package com.example.curdAPI.services;

import com.example.curdAPI.entity.PersonsEntity;
import com.example.curdAPI.repository.PersonsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonsServiceIml implements PersonsServices{

    @Autowired
    private PersonsRepo personsRepo;

    @Override
    public List<PersonsEntity> getAllPersonsEntity() {
        return personsRepo.findAll();
    }

    @Override
    public PersonsEntity getPersonsEntityById(int id) {
        return personsRepo.findById(id).orElse(null);
    }

    @Override
    public PersonsEntity savePersonsEntity(PersonsEntity personsEntity) {
        return personsRepo.save(personsEntity);
    }

    @Override
    public void deletePersonsEntity(int id) {
        personsRepo.deleteById(id);

    }
}
