package com.example.curdAPI.services;
import com.example.curdAPI.entity.PersonsEntity;
import java.util.List;

public interface PersonsServices {
     List<PersonsEntity>getAllPersonsEntity();
     PersonsEntity getPersonsEntityById(int id);
     PersonsEntity savePersonsEntity(PersonsEntity personsEntity);
     void deletePersonsEntity(int id);

}
