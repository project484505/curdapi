package com.example.curdAPI.repository;


import com.example.curdAPI.entity.PersonsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonsRepo extends JpaRepository<PersonsEntity, Integer> {
}
