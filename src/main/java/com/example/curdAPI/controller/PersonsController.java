package com.example.curdAPI.controller;

import com.example.curdAPI.entity.PersonsEntity;
import com.example.curdAPI.services.PersonsServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class PersonsController {

    @Autowired
    private PersonsServices personsServices;

    @GetMapping("/persons")
    public List<PersonsEntity> getAllPersons(){
        return personsServices.getAllPersonsEntity();

    }
    @GetMapping("/person/{id}")
    public PersonsEntity getPersonsById(@PathVariable int id){
        return personsServices.getPersonsEntityById(id);
    }

    @PostMapping("/persons")
    public PersonsEntity createPerson(@RequestBody PersonsEntity personsEntity){
        return personsServices.savePersonsEntity(personsEntity);
    }

    @PutMapping("/persons/{id}")
    public PersonsEntity updatePerson(@PathVariable int id, @RequestBody PersonsEntity personsEntity){
        personsEntity.setId(id);
        return personsServices.savePersonsEntity(personsEntity);

    }
    @DeleteMapping("/persons/{id}")
    public void deletePerson(@PathVariable int id){
        personsServices.deletePersonsEntity(id);
    }

}
